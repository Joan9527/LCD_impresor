/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lcd;

import java.util.Scanner;

/**
 *
 * @author Joan Camilo
 */
public class Lcd {
    
    //Caracteres
    public static final String caracterVertical = "|";
    public static final  String caracterHorizontal = "-";
    
    //Segmentos de cada número
    String[] matrizSegmentos = new String[10]; 
           
    /**
     * Método encargado de inicializar los valores de la matriz con los que se forman los números
     * Esquema de segmentos:
     *   a
     *   -
     * b| |d
     *  c-
     * d| |f
     *  e-
     */
    
    public void inicializarMatriz(){    
        matrizSegmentos[0] = "a,b,d,e,f,g"; 
        matrizSegmentos[1] = "b,d"; 
        matrizSegmentos[2] = "a,g,c,d,e"; 
        matrizSegmentos[3] = "a,g,f,c,e";
        matrizSegmentos[4] = "b,c,g,f";
        matrizSegmentos[5] = "a,b,c,f,e";
        matrizSegmentos[6] = "a,b,d,e,f,c";
        matrizSegmentos[7] = "a,g,f";
        matrizSegmentos[8] = "a,b,c,d,e,f,g";
        matrizSegmentos[9] = "a,b,c,g,f";   
    }
    
    /**
     * Este método se encarga de verificar que las entradas dadas por 
     * el usuario sean correctas.
     * @size El size dado por el usuario
     * @numero El numero que será mostrado en pantalla
     * @return true si las entradas son correctas, false si son incorrectas
     */
    
    public boolean verificarEntradas(String[] entradas){
        boolean entradasCorrectas = false;
        if(entradas.length<2){
            return false;
        }
        String cadenaSize = entradas[0];
        String numero = entradas[1];
        if(esNumero(cadenaSize) &&  esNumero(numero)){
            entradasCorrectas = true;
        }                        
        return entradasCorrectas;
    }
    
    /**
     * Este metodo se encarga de verificar que una cadena
     * pueda ser convertida a número
     * @cadena La cadena a evaluar
     * @return True si puede ser convertida a número, false si no
     */
    
    public boolean esNumero(String cadena){
        boolean esNumero = false;
        try{
            Integer.parseInt(cadena);
            esNumero = true;
        }catch(NumberFormatException exp){
            
        }
        return esNumero;
    }
        
    /**
     * Método encargado de crear la cadena en la que se muestra un número
     * @size El tamaño del número
     * @size El número a imprimir
     * @return La cadena que muestra el número especificado
     */
    
    public String crearCadena (int size, String numeros){
        String cadena = "";
        int alto = size*2 + 3;
        int ancho  = (size + 2);        
        int posicionNumero = 0;        
        
        for(int filas = 0; filas < (alto); filas++){
            String cadenaNumero = Character.toString(numeros.charAt(posicionNumero));
            int numero = Integer.parseInt(cadenaNumero);            
            
            for(int columnas = 0; columnas < ancho; columnas++){                
                //Segmento a 
                if(filas == 0){
                    if(matrizSegmentos[numero].contains("a") && columnas > 0 && columnas < ancho - 1){
                       cadena = cadena + "-";
                    }else{
                        cadena = cadena + " ";     
                    }
                // Segmentos b y g    
                }else if(filas > 0 && filas <= size){
                    if(columnas==0){
                        if(matrizSegmentos[numero].contains("b")){
                            cadena = cadena + "|";
                        }else{
                            cadena = cadena + " ";     
                        }
                    }
                    else if(columnas==ancho-1){
                        if(matrizSegmentos[numero].contains("g")){
                            cadena = cadena + "|";
                        }else{
                            cadena = cadena + " ";     
                        }
                    }else{
                        cadena = cadena + " ";     
                    }
                //Segmento c
                }else if(filas == size + 1 ){
                    if(matrizSegmentos[numero].contains("c") && columnas > 0 && columnas < ancho - 1){
                            cadena = cadena + "-";
                    }else{
                        cadena = cadena + " ";     
                    }
                //Segmentos d y f
                }else if(filas > size + 1 && filas < alto - 1){
                    if(columnas==0){
                        if(matrizSegmentos[numero].contains("d")){
                            cadena = cadena + "|";
                        }else{
                            cadena = cadena + " ";     
                        }   
                    }
                    else if(columnas==ancho-1){
                        if(matrizSegmentos[numero].contains("f")){
                            cadena = cadena + "|";
                        }else{
                            cadena = cadena + " ";     
                        }
                    }else{
                        cadena = cadena + " ";     
                    }
                }
                //Segmento e
                else if(filas == alto - 1){
                    if(matrizSegmentos[numero].contains("e") && columnas > 0 && columnas < ancho - 1){
                            cadena = cadena + "-";
                    }else{
                        cadena = cadena + " ";
                    }
                }
                
                cadena = cadena + " ";           
                if(columnas == ancho - 1){
                    if(posicionNumero < numeros.length()-1){
                        columnas = -1;
                        posicionNumero++;
                        cadenaNumero = Character.toString(numeros.charAt(posicionNumero));
                        numero = Integer.parseInt(cadenaNumero);
                        cadena = cadena+" ";
                    }else{
                        posicionNumero=0;                        
                        cadenaNumero = Character.toString(numeros.charAt(posicionNumero));
                    }
                }
                
            }//columnas            
            cadena = cadena+" \n";
        }//filas
        System.out.print(cadena);
        System.out.println("Fin");
        return cadena;
    }        
    
    
    /**
     * Método encargado del proceso general: la captura de las entradas y
     * la impresión de los números 
     */
    
    public void imprimirNumeros (){
        Scanner scanner = new Scanner(System.in);
        String entrada = "";
        do{  
            
            System.out.println("Digite una entrada : size,numero");
            entrada = scanner.next();            
            String[] pares = entrada.split(",");
            
            if(verificarEntradas(pares)){
                crearCadena(Integer.parseInt(pares[0]), pares[1]);            
            }else{
                System.out.println("La entrada especificada no es correcta, el formato debe ser: size,numero");
            }                        
            
        }while(!entrada.equalsIgnoreCase("0,0"));
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Lcd lcd = new Lcd();
        lcd.inicializarMatriz();
        lcd.imprimirNumeros();
    }
    
}
